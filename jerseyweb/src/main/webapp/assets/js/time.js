/**
 * 
 */

var time = {
	version: 0,
	dev: false,
	init: function(isDev){
		this.dev = isDev;
		if (this.dev) this.uri = 'http://localhost:8080';
		else
		{
			this.uri = 'https://test.hyuns.io';
			
			// 마우스 오른쪽 클릭 막기
			window.oncontextmenu = function()
			{
			return false;
			};
		}
	},
	start: function() {
		this.$o001 = $('#o001');
		this.$o002 = $('#o002');
		this.$o003 = $('#o003');
		this.$loading = $('#loading');
		
		this.$loading.hide();
	},
	finish: function() {
//		if (null != this.b001){
//			this.b001.finish();
//		}
//		
//		this.start(this.organizationId);
	}
};
time.init(true);
//time.init(false);


/**
 * time Util
 */ 
time.util = {
	version: 1
	, hello: function(){
		return 'time.util';
	}
	, nvl: function(s){
		if (null==s) return '';
		else return s;
	}
	, httpReqJson: function(context, path, json, ignoreFail){
		if (time.dev) console.log(time.uri + path);
		return $.ajax({
			url: time.uri + path
			, method: 'POST'
			, contentType: 'application/json; charset=utf-8'
			, data: JSON.stringify(json)
			, cache: false
			, context: context
			, Accept: 'application/vnd.time.0+json'
		}).done(function(res){
			if (time.dev) console.log(res);
		}).fail(function(jqXHR, textStatus, errorThrown){
			if (jqXHR.status == "303" && jqXHR.responseJSON.do == "signInAgain") {
				this.finish();
				time.finish();
//				alert("로그아웃 처리 됩니다.");
				$(location).attr('href', '/');
			}
			else
			{
				if (time.dev) console.log('ajax_fail');
				if ( ! ignoreFail)
				{
					alert('ajax_fail');
				}
			}
		}).always(function(){
		});
	}
	, httpReqMultipart: function(context, path, formData, ignoreFail){
		if (time.dev) console.log(time.uri + path);
		return $.ajax({
			url: time.uri + path
			, method: 'POST'
			, enctype: 'multipart/form-data'
			, data: formData
			, contentType: false
			, processData: false
			, cache: false
			, context: context
			, Accept: 'application/vnd.time.0+json'
		}).done(function(res){
			if (time.dev) console.log(res);
		}).fail(function(jqXHR, textStatus, errorThrown){
			if ( jqXHR.responseJSON.error != null ) {
				if (time.dev) console.log('ajax_fail');
				alert('Error Message: ' + jqXHR.responseJSON.error.msg);
			}
			else {
				alert(jqXHR + textStatus);
			}
		}).always(function(){
			
		});
	}
	, getHttpReq: function(context, path, data, ignoreFail){
		if (time.dev) console.log(time.uri + path);
		return $.ajax({
			url: time.uri + path
			, method: 'GET'
			, data: data
			, contentType: 'application/json; charset=utf-8'
			, cache: false
			, context: context
			, Accept: 'application/vnd.time.0+json'
		}).done(function(res){
			if (time.dev) console.log(res);
		}).fail(function(jqXHR, textStatus, errorThrown){
			if ( jqXHR.responseJSON.error != null ) {
				if (time.dev) console.log('ajax_fail');
				alert('Error Message: ' + jqXHR.responseJSON.error.msg);
			}
		}).always(function(){
			
		});
	}
	, oGetHttpReq: function(context, path, data){
		if (time.dev) console.log(time.uri + path);
		return $.ajax({
			url: time.uri + path
			, data: data
			, cache: false
			, context: context
			, Accept: 'application/vnd.time.0+json'
		}).done(function(res){
			if (time.dev) console.log(res);
		}).fail(function(jqXHR, textStatus, errorThrown){
			if (jqXHR.status == "303" && jqXHR.responseJSON.do == "signInAgain") {
				this.finish();
//				time.finish();
				alert("토큰이 만료되었거나 값이 없어 로그아웃 처리 됩니다.");
			}
			else {
				if (time.dev) console.log('ajax_fail');
				if (jqXHR.responseJSON) 
				{
					if (jqXHR.responseJSON.msg == "다시 로그인 해주세요.")
					{
						this.finish();
//						time.finish();
					}
					else alert(jqXHR.responseJSON.msg);
				}
			}
		}).always(function(){
		});
	}
	, putHttpReq: function(context, path, json){
		if (time.dev) console.log(time.uri + path);
		return $.ajax({
			url: time.uri + path	
			, method: 'PUT'
			, contentType: 'application/json; charset=utf-8'
			, data: JSON.stringify(json)
			, cache: false
			, context: context
			, Accept: 'application/vnd.time.0+json'
		}).done(function(res){
			if (time.dev) console.log(res);
		}).fail(function(jqXHR, textStatus, errorThrown){
			if (jqXHR.status == "303" && jqXHR.responseJSON.do == "signInAgain") {
				this.finish();
				time.finish();
				alert("토큰이 만료되었거나 값이 없어 로그아웃 처리 됩니다.");
			}
			else {
				if (time.dev) console.log('ajax_fail');
				if (jqXHR.responseJSON) 
				{
					if (jqXHR.responseJSON.msg == "다시 로그인 해주세요.")
					{
						this.finish();
//						time.finish();
					}
					else alert(jqXHR.responseJSON.msg);
				}
			}
			
		}).always(function(){
		});
	}
	, uiClear: function(o){
		if (o){
			if (o.ui){
				o.ui.hide();
				o.ui.find('*').off();
				delete o.ui.$;
				delete o.ui;
			}
		}
	}
	, uiSet: function(o,$o){
		if ($o.$) $o.$.finish();
		$o.$ = o;
		o.ui = $o;
	}
	, idPasswordToHashBase64: function(id, pw){
		var sha256 = forge.md.sha256;
		var idHashBuffer = sha256.create().update(id).digest();
		var passwordHashBuffer = sha256.create().update(pw).digest();
		idHashBuffer.putBuffer(passwordHashBuffer);
		var idPasswordHashBuffer = sha256.create().update(idHashBuffer.getBytes()).digest();
		return forge.util.binary.base64.encode(forge.util.binary.hex.decode(idPasswordHashBuffer.toHex()));
	}
	, unixTimeToString: function(unixTime,format,noValueLabel){
		if (0==unixTime){
			if (noValueLabel) return noValueLabel;
			else return '';
		}else{
			var date = new Date(unixTime);
			var year = date.getFullYear();
			var month = ('0'+(date.getMonth()+1)).substr(-2);
			var day = ('0'+date.getDate()).substr(-2);
			var hour = ('0'+date.getHours()).substr(-2);
			var minutes = ('0'+date.getMinutes()).substr(-2);
			var seconds = ('0'+date.getSeconds()).substr(-2);
			
			switch (format){
			case 0:
				return year+'-'+month+'-'+day;
			case 1:
				return year+'-'+month+'-'+day+'<br>'+hour+':'+minutes+':'+seconds;
			case 2:
			default:
				return year+'-'+month+'-'+day+' '+hour+':'+minutes+':'+seconds;
			}
			
		}
	}
	, uriParam: function(name){
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
	}
	, imageResizeAndSetImgSrc2: function(src,max,$img){//https://stackoverflow.com/questions/15491193/getting-width-height-of-an-image-with-filereader
		var img = new Image();
		img.src = src;
		img.onload=function(){
			var w=0, h=0;
			var sx=0, sy=0;
			if (img.width > img.height){
				h = max;
				w = img.width * max / img.height;
				sx = 0;
				sy = (w - max)/2;
			}
			else{
				w = max;
				h = img.height * max / img.width;
				sx = (w - max)/2;
				sy = 0;
			}
			
			//
			var oc = document.createElement('canvas'), octx = oc.getContext('2d');

			oc.width = w;//img.width*0.5;
			oc.height = h;//img.height*0.5;
			octx.drawImage(img, 0, 0, oc.width, oc.height);
			
			//
			octx.drawImage(oc, 0, 0, w, h);//oc.width*0.5, oc.height*0.5);
			
			//
			var canvas = document.createElement('canvas');
			var ctx = canvas.getContext('2d');
			canvas.width = max;
			canvas.height = max;
			ctx.drawImage(oc, sx, sy, max, max, 0, 0, canvas.width, canvas.height);
			
			$img.attr('src', canvas.toDataURL('image/jpeg', 0.9)).show();
		};
	}
	, dataURItoBlob: function(dataURI){//https://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
		var byteString = atob(dataURI.split(',')[1]);
		var ab = new ArrayBuffer(byteString.length);
		var ia = new Uint8Array(ab);
		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}
		return new Blob([ab], { type: 'image/jpeg' });
	}
	, fileSizeConvert: function(bytes, decimals, binaryUnits) {
		if (bytes == 0) {
			return '0 Bytes';
		}
		var unitMultiple = (binaryUnits) ? 1024 : 1000;
		var unitNames = (unitMultiple === 1024) ? // 1000 bytes in 1 Kilobyte
													// (KB) or 1024 bytes for
													// the binary version (KiB)
		[ 'Bytes', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB' ] : [
				'Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB' ];
		var unitChanges = Math.floor(Math.log(bytes) / Math.log(unitMultiple));
		return parseFloat((bytes / Math.pow(unitMultiple, unitChanges))
				.toFixed(decimals || 0))
				+ ' ' + unitNames[unitChanges];

	}
	, getOrganizationEncryptTypeText : function(type) {
		switch (type.toString()) {
		case '2':
			return 'AES256';
		case '3':
			return 'SEED256';
		default:
			return '?';
		}
	}
	, generateRandomString : function(length) {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
		var string_length = length;
		var randomstring = '';
		
		for (var i = 0; i < string_length; i++) {
			var rnum = Math.floor(Math.random() * chars.length);
			randomstring += chars.substring(rnum, rnum+1);
		}
		
		return randomstring;
	}
	, stringJsonArrayToJsonArray : function(stringArray) {
		var jsonArray = new Array();
		
		for (var i = 0; i < stringArray.length; i++) {
			jsonArray[i] = JSON.parse(stringArray[i]);
		}
		
		return jsonArray;
	}
	, valueConsoleLogging : function(valueName, value) {
		console.log(valueName + ": " + value);
	}
	, popupCenterWindow (url, title, width, height) 
	{
		var curX = window.screenLeft;
		var curY = window.screenTop;
		var curWidth = document.body.clientWidth;
		var curHeight = Math.max(window.innerHeight, document.body.clientHeight);
		
		
		console.log("curX: " + curX);
		console.log("curY: " + curY);
		console.log("curWidth: " + curWidth);
		console.log("curHeight: " + curHeight);
		
		  
		var nLeft = curX + (curWidth / 2) - (width / 2);
		var nTop = curY + (curHeight / 2) - (height / 2);
		
		console.log("nLeft: " + nLeft);
		console.log("nTop: " + nTop);

		var strOption = "";
		strOption += "left=" + nLeft + "px,";
		strOption += "top=" + nTop + "px,";
		strOption += "width=" + width + "px,";
		strOption += "height=" + height + "px,";
		strOption += "toolbar=no,menubar=no,location=no,";
		strOption += "resizable=yes,status=yes";
		  
		var winObj = window.open(url, title, strOption);
		
		if (winObj == null) {
		    alert("팝업 차단을 해제해주세요.");
		    return false;
		}
	}
	, getCurrentDate: function(format) {
		var date = new Date();
		var year = date.getFullYear();
		var month = ('0'+(date.getMonth()+1)).substr(-2);
		var day = ('0'+date.getDate()).substr(-2);
		var hour = ('0'+date.getHours()).substr(-2);
		var minutes = ('0'+date.getMinutes()).substr(-2);
		var seconds = ('0'+date.getSeconds()).substr(-2);
		
		switch (format) {
			case 0:
				return year+'-'+month+'-'+day;
			case 1:
				return year+'-'+month+'-'+day+'<br>'+hour+':'+minutes+':'+seconds;
			case 2:
			default:
				return year+'-'+month+'-'+day+' '+hour+':'+minutes+':'+seconds;
		}
	}
	, getCookie: function(name) {
		var value = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
		return value? value[2] : null;
	}
	, setCookie : function(name, value, timeValue) {
		if (time.dev) document.cookie = name + '=' + value + '; Path=/page/main; Expires=' + timeValue;
		else
		{
//			document.cookie = name + '=' + value + '; Path=/page/main; secure; httpOnly;';
			document.cookie = name + '=' + value + '; Path=/page/main; secure; Expires=' + timeValue;
		}
	}
	, deleteCookie : function(name) {
		document.cookie = name +'='+ '' +'; Path=/page/main; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}
	, addMinutes : function addMinute(now, addTimeValue) {
		return new Date(now.setMinutes(now.getMinutes() + addTimeValue));
	}
	, addDate : function addDate(now, addTimeValue) {
		return new Date(now.setDate(now.getDate() + addTimeValue));
	}
	, getRandomColorHex() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
			}
		return color;
	}
	, getRandomColorRGB() {
		var o = Math.round, r = Math.random, s = 255;
	    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
	}
};



//------------------------------------------------------------------------------------------------------------------------------
/**
 * o001 - sign in
 */
function o001(){
	time.$o001.$ = this;
	this.ui = time.$o001;
	this.keypressFlag = true;
}
o001.prototype.start = function() {
	var o001 = this;
	var ui = this.ui;
	
	ui.find('#submitbtn').on("click", this.onSubmitClick.bind(this));
	ui.find('#forgotpwbtn').on("click", this.onReadyClick.bind(this));
	ui.find('#google').on("click", this.onReadyClick.bind(this));
	
	ui.find('#accountemail').on("keypress", function() {
		if (window.event.keyCode == 13){
			ui.find('#accountpw').focus();
		}
	});
	
	// pw Enter / Infinite Input Prevention
	ui.find('#accountpw').on("keypress", function() {
		if (window.event.keyCode == 13){
			if(o001.keypressFlag){
				o001.keypressFlag = false;
				o001.onSubmitClick();
			}
		}
	});
}
o001.prototype.onSubmitClick=function(){
	var o001 = this;
	var $email = this.ui.find('#accountemail');
	var $pw = this.ui.find('#accountpw');
	
	var email = $email.val();
	var pw = $pw.val();
	
	if (0 == email.length){
		alert('Please enter email.');
		o001.keypressFlag = true;
		$email.focus();
	}
	else if (0 == pw.length){
		alert('Please enter password.');
		o001.keypressFlag = true;
		$pw.focus();
	}
	else{
		{
			this.callSignIn(email, time.util.idPasswordToHashBase64(email, pw));
		}
	}
};

o001.prototype.onReadyClick=function(){
	alert("Still in preparation. I'm sorry!");
};


o001.prototype.callSignIn=function(email, passwordHashHex){
	var o001 = this;
	var ui = o001.ui;
	
	this.ui.find('#submitbtn').prop("disabled", true);
	
	var formData = new FormData();
	formData.append('accountemail', email);
	formData.append('accountpw', passwordHashHex);
	
	//
//	var l = new loading().start();
	time.util.httpReqMultipart(this, '/account/service/signin', formData).done(function(res, textStatus, jqXHR){
		time.util.setCookie('id', email, null);
		
		const now = new Date(Date.now());
		console.log(now);

		const at = time.util.addMinutes(new Date(), 30);
		console.log(at)

		let rt = time.util.addDate(new Date(), 1);
		console.log(rt)
		
		time.util.setCookie('id', res.id, at);

		console.log("httpReqMultipart: Success !!");
		$(location).attr('href', '/page/main');
	}).always(function(){
		this.ui.find('#submitbtn').prop("disabled", false);
		o001.keypressFlag = true;
		l.finish();
	});
	
};




//------------------------------------------------------------------------------------------------------------------------------
/**
 * o002 - main
 */
function o002() {
	time.$o002.$ = this;
	this.ui = time.$o002;
}
o002.prototype.start = function() {
	var o002 = this;
	var ui = this.ui;
	
	var getId = time.util.getCookie('id');
	console.log('id: ' + getId);
	
//	ui.find('#user').text(getId);
	ui.find('#dataDate').text('Data syncl time: ' + time.util.getCurrentDate(2));
	
	
	ui.find('#signout').on("click", this.onSignOutClick.bind());
	
	
}
o002.prototype.onSignOutClick = function() {

	time.util.httpReqJson(this, '/account/service/signout').done(function(res, textStatus, jqXHR){
		time.util.deleteCookie('id');

		alert("Sign Out");
	}).always(function(){
		$(location).attr('href', '/');
	});

};



//------------------------------------------------------------------------------------------------------------------------------
/**
 * o003 - sign up
 */
function o003() {
	time.$o003.$ = this;
	this.ui = time.$o003;
	this.keypressFlag = true;
}
o003.prototype.start = function() {
	var o003 = this;
	var ui = this.ui;
	
	ui.find('#submitbtn').on("click", this.onSubmitClick.bind(this));
	
	ui.find('#accountemail').on("keypress", function() {
		if (window.event.keyCode == 13){
			ui.find('#accountpw').focus();
		}
	});
	
	// pw Enter / Infinite Input Prevention
	ui.find('#accountpw').on("keypress", function() {
		if (window.event.keyCode == 13){
			if(o003.keypressFlag){
				o003.keypressFlag = false;
				o003.onSubmitClick();
			}
		}
	});
}
o003.prototype.onSubmitClick=function(){
	var o003 = this;
	var $email = this.ui.find('#accountemail');
	var $pw = this.ui.find('#accountpw');
	
	var email = $email.val();
	var pw = $pw.val();
	
	if (0 == email.length){
		alert('Please enter email.');
		o003.keypressFlag = true;
		$email.focus();
	}
	else if (0 == pw.length){
		alert('Please enter password.');
		o003.keypressFlag = true;
		$pw.focus();
	}
	else{
		{
			this.callSignUp(email, time.util.idPasswordToHashBase64(email, pw));
		}
	}
};


o003.prototype.callSignUp=function(email, passwordHashHex){
	var o003 = this;
	var ui = o003.ui;
	
	this.ui.find('#submitbtn').prop("disabled", true);
	
	var formData = new FormData();
	formData.append('accountemail', email);
	formData.append('accountpw', passwordHashHex);
	
	//
	time.util.httpReqMultipart(this, '/account/service/signup', formData).done(function(res, textStatus, jqXHR){
		
		console.log("httpReqMultipart: Success !!");
		alert("Success!");
		$(location).attr('href', '/');
	}).always(function(){
		this.ui.find('#submitbtn').prop("disabled", false);
		o003.keypressFlag = true;
	});
	
};






function loading(){
	time.$loading.$ = this;
	this.ui = time.$loading;
}
loading.prototype.start=function(){
	var loading = this;
	var ui = this.ui;
	
//	ui.find('#loading').show();
	
//	this.timerId = setInterval(this.updateDot, 500);

	this.ui.appendTo('body');
	this.ui.show();
	
	return this;
};
loading.prototype.finish=function(){
	time.util.uiClear(this);
//	tb.$loading.hide();
//	clearInterval(this.timerId);
};



