package hyuns.project.jerseyweb.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * <pre> 
 * hyuns.project.jerseyweb.api.response
 * MessageResponse.java
 * </pre>
 *
 * @since 1.0
 * @date Mar 1, 2020
 * @author James Choi
 */
public class MessageResponse implements JerseyResourceResponse {
	
	private String msg;
	
	/**
	 * 
	 * <pre>
	 * @constructor MessageResponse (String)
	 * </pre>
	 *
	 * @param msg
	 * @author James Choi
	 */
	public MessageResponse(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 
	 * <pre>
	 * @method getMsg
	 * </pre>
	 * 
	 * @return
	 * @author James Choi
	 */
	@JsonProperty
	public String getMsg() {
		return msg;
	}
	
}
