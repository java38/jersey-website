package hyuns.project.jerseyweb.api.resource;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import hyuns.project.jerseyweb.api.resource.account.AccountService;
import hyuns.project.jerseyweb.api.resource.account.AccountSignUpParam;

@Path("account")
public class AccountResource extends AccountService {
	
	private static final Logger log = LogManager.getLogger(AccountResource.class);
	
	@Path("signup")
	@POST
	@Produces(APPLICATION_JSON_VERSION_0)
	@Override
	public Response signUp(@Context HttpServletRequest request, 
						   AccountSignUpParam param) 
	{
		log.info("[Resource Path] ==> signup");
		return super.signUp(request, param);
	}
	
}
