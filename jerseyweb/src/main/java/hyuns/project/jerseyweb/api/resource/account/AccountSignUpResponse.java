package hyuns.project.jerseyweb.api.resource.account;

import java.util.Locale;

import com.fasterxml.jackson.annotation.JsonProperty;

import hyuns.project.jerseyweb.api.response.ErrorResponse;
import hyuns.project.jerseyweb.api.response.JerseyResourceResponse;
import hyuns.project.jerseyweb.api.values.ResultCode;

public class AccountSignUpResponse implements JerseyResourceResponse {

	private ErrorResponse error;
	
	/**
	 * 
	 * <pre>
	 * @constructor AccountSignUpResponse
	 * @contents error is null = success
	 * </pre>
	 *
	 * @author James Choi
	 */
	public AccountSignUpResponse() {
		this.error = null;
	}
	
	/**
	 * 
	 * <pre>
	 * @constructor AccountSignUpResponse
	 * @contents error is not null = fail
	 * </pre>
	 *
	 * @param locale
	 * @param resultCode
	 * @author James Choi
	 */
	public AccountSignUpResponse(Locale locale, ResultCode resultCode) {
		this.error = new ErrorResponse(locale, resultCode);
	}
	
	@JsonProperty
	public ErrorResponse getError() {
		return error;
	}
	
}
