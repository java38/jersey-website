package hyuns.project.jerseyweb.api.response;

import java.util.Locale;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import hyuns.project.jerseyweb.api.values.ResultCode;

/**
 * 
 * <pre> 
 * hyuns.project.jerseyweb.api.response
 * ErrorResponse.java
 * </pre>
 *
 * @since 1.0
 * @date Mar 1, 2020
 * @author James Choi
 */
@JsonAutoDetect
public class ErrorResponse implements JerseyResourceResponse {

	private String code;
	private String msg;
	
	/**
	 * 
	 * <pre>
	 * @constructor ErrorResponse (String, String)
	 * </pre>
	 *
	 * @param code
	 * @param msg
	 * @author James Choi
	 */
	public ErrorResponse(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	
	/**
	 * 
	 * <pre>
	 * @constructor ErrorResponse (Locale, ApplicationResultCode)
	 * </pre>
	 *
	 * @author James Choi
	 * @param locale
	 * @param code
	 */
	public ErrorResponse(Locale locale, ResultCode resultCode) {
		if (locale == null) {
			locale = Locale.ENGLISH;
		}
		
		this.code = resultCode.getCode();
//		this.msg = lo
	}
	
	/**
	 * 
	 * <pre>
	 * @constructor ErrorResponse (null)
	 * </pre>
	 *
	 * @author James Choi
	 */
	public ErrorResponse() {
		this.code = null;
		this.msg = null;
	}
	
	/**
	 * 
	 * <pre>
	 * @method getCode
	 * 
	 * </pre>
	 * 
	 * @return
	 * @author James Choi
	 */
	@JsonProperty
	public String getCode() {
		return code;
	}
	
	/**
	 * 
	 * <pre>
	 * @method getMsg
	 * 
	 * </pre>
	 * 
	 * @return
	 * @author James Choi
	 */
	@JsonProperty
	public String getMsg() {
		return msg;
	}
	
	
}
