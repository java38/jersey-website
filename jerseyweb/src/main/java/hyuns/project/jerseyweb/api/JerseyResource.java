package hyuns.project.jerseyweb.api;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.server.mvc.Viewable;

import hyuns.project.jerseyweb.api.response.JerseyResourceResponse;
import hyuns.project.jerseyweb.api.response.MessageResponse;

public class JerseyResource {
	
	private static final Logger log = LogManager.getLogger(JerseyResource.class);
	
	public static final String APPLICATION_JSON_VERSION_0 = "application/vnd.james.0+json";
	public static final String APPLICATION_JSON_VERSION_1 = "application/vnd.james.1+json";
	public static final String APPLICATION_JSON_VERSION_2 = "application/vnd.james.2+json";
	
	/**
	 * 
	 * <pre>
	 * @method defaultSuccess
	 * @statuscode 200 [Success]
	 * @contents 요청이 성공적으로 되었습니다. 성공의 의미는 HTTP 메소드에 따라 달라집니다:
	 * GET: 리소스를 불러와서 메시지 바디에 전송되었습니다.
	 * HEAD: 개체 해더가 메시지 바디에 있습니다.
	 * PUT 또는 POST: 수행 결과에 대한 리소스가 메시지 바디에 전송되었습니다.
	 * TRACE: 메시지 바디는 서버에서 수신한 요청 메시지를 포함하고 있습니다.
	 * </pre>
	 * 
	 * @return
	 * @author James Choi
	 */
	public Response defaultSuccess() {
		return Response.status(Response.Status.OK).build();
	}
	
	/**
	 * 
	 * <pre>
	 * @method defaultSuccess (JerseyResourceResponse)
	 * @statuscode 200 [Success]
	 * @contents 요청이 성공적으로 되었습니다. 성공의 의미는 HTTP 메소드에 따라 달라집니다:
	 * GET: 리소스를 불러와서 메시지 바디에 전송되었습니다.
	 * HEAD: 개체 해더가 메시지 바디에 있습니다.
	 * PUT 또는 POST: 수행 결과에 대한 리소스가 메시지 바디에 전송되었습니다.
	 * TRACE: 메시지 바디는 서버에서 수신한 요청 메시지를 포함하고 있습니다.
	 * </pre>
	 * 
	 * @param response
	 * @return
	 * @author James Choi
	 */
	public Response defaultSuccess(JerseyResourceResponse response) {
		Response res = Response.status(Response.Status.OK)
							   .type(MediaType.APPLICATION_JSON)
							   .entity(response)
							   .build();
		
		log.debug("[Response] defaultSuccess(jersey resource object) ==> " + res.getEntity());
		
		return res;
	}
	
	
	/**
	 * 
	 * <pre>
	 * @method defaultSuccess (Viewable)
	 * @statuscode 200 [Success]
	 * @contents 요청이 성공적으로 되었습니다. 성공의 의미는 HTTP 메소드에 따라 달라집니다:
	 * GET: 리소스를 불러와서 메시지 바디에 전송되었습니다.
	 * HEAD: 개체 해더가 메시지 바디에 있습니다.
	 * PUT 또는 POST: 수행 결과에 대한 리소스가 메시지 바디에 전송되었습니다.
	 * TRACE: 메시지 바디는 서버에서 수신한 요청 메시지를 포함하고 있습니다.
	 * </pre>
	 * 
	 * @param viewable
	 * @return
	 * @author James Choi
	 */
	public Response defaultSuccess(Viewable viewable) {
		Response res = Response.status(Response.Status.OK)
							   .type(MediaType.APPLICATION_JSON)
							   .entity(viewable)
							   .build();

		log.debug("[Response] defaultSuccess(move page) ==> " + viewable.getTemplateName());
		
		return res;
	}
	
	// ---------------------------------------------------------------------------------------
	
	/**
	 * 
	 * <pre>
	 * @method createdSuccess
	 * @statuscode 201 [Created]
	 * @contents 요청이 성공적이었으며 그 결과로 새로운 리소스가 생성되었습니다. 
	 * 이 응답은 일반적으로 POST 요청 또는 일부 PUT 요청 이후에 따라옵니다.
	 * </pre>
	 * 
	 * @param response
	 * @return
	 * @author James Choi
	 */
	public Response createdSuccess(JerseyResourceResponse response) {
		return Response.status(Response.Status.CREATED)
					   .type(MediaType.APPLICATION_JSON)
					   .entity(response)
					   .build();
	}
	
	/**
	 * 
	 * <pre>
	 * @method createdSuccess
	 * @statuscode 201 [Created]
	 * @contents 요청이 성공적이었으며 그 결과로 새로운 리소스가 생성되었습니다. 
	 * 이 응답은 일반적으로 POST 요청 또는 일부 PUT 요청 이후에 따라옵니다.
	 * </pre>
	 * 
	 * @param msg
	 * @return
	 * @author James Choi
	 */
	public Response createdSuccess(String msg) {
		return Response.status(Response.Status.CREATED)
					   .type(MediaType.APPLICATION_JSON)
					   .entity(new MessageResponse(msg))
					   .build();
	}
	
	/**
	 * 
	 * <pre>
	 * @method createdSuccess
	 * @statuscode 201 [Created]
	 * @contents 요청이 성공적이었으며 그 결과로 새로운 리소스가 생성되었습니다. 
	 * 이 응답은 일반적으로 POST 요청 또는 일부 PUT 요청 이후에 따라옵니다.
	 * </pre>
	 * 
	 * @return
	 * @author James Choi
	 */
	public Response createdSuccess() {
		return Response.status(Response.Status.CREATED)
					   .type(MediaType.APPLICATION_JSON)
					   .build();
	}
	
	
	// ---------------------------------------------------------------------------------------
	
	/**
	 * 
	 * <pre>
	 * @method noContent
	 * @statuscode 204 [No Content]
	 * @contents 요청에 대해서 보내줄 수 있는 콘텐츠가 없지만, 헤더는 의미있을 수 있습니다. 
	 * 사용자-에이전트는 리소스가 캐시된 헤더를 새로운 것으로 업데이트 할 수 있습니다.
	 * </pre>
	 * 
	 * @param msg
	 * @return
	 * @author James Choi
	 */
	public Response noContent(String msg) {
		return Response.status(Response.Status.NO_CONTENT)
					   .type(MediaType.APPLICATION_JSON)
					   .entity(new MessageResponse(msg))
					   .build();
	}
	
	/**
	 * 
	 * <pre>
	 * @method noContent
	 * @statuscode 204 [No Content]
	 * @contents 요청에 대해서 보내줄 수 있는 콘텐츠가 없지만, 헤더는 의미있을 수 있습니다. 
	 * 사용자-에이전트는 리소스가 캐시된 헤더를 새로운 것으로 업데이트 할 수 있습니다.
	 * </pre>
	 * 
	 * @param response
	 * @return
	 * @author James Choi
	 */
	public Response noContent(JerseyResourceResponse response) {
		return Response.status(Response.Status.NO_CONTENT)
					   .type(MediaType.APPLICATION_JSON)
					   .entity(response)
					   .build();
	}
	
	// ---------------------------------------------------------------------------------------
	
	/**
	 * 
	 * <pre>
	 * @method badRequest
	 * @statuscode 400 [Bad Request]
	 * @contents 이 응답은 잘못된 문법으로 인하여 서버가 요청을 이해할 수 없음을 의미합니다.
	 * </pre>
	 * 
	 * @param msg
	 * @return
	 * @author James Choi
	 */
	public Response badRequest(String msg) {
		return Response.status(Response.Status.BAD_REQUEST)
					   .type(MediaType.APPLICATION_JSON)
					   .entity(new MessageResponse(msg))
					   .build();
	}
	
	/**
	 * 
	 * <pre>
	 * @method badRequest
	 * @statuscode 400 [Bad Request]
	 * @contents 이 응답은 잘못된 문법으로 인하여 서버가 요청을 이해할 수 없음을 의미합니다.
	 * </pre>
	 * 
	 * @param response
	 * @return
	 * @author James Choi
	 */
	public Response badRequest(JerseyResourceResponse response) {
		return Response.status(Response.Status.BAD_REQUEST)
					   .type(MediaType.APPLICATION_JSON)
					   .entity(response)
					   .build();
	}
	
	// ---------------------------------------------------------------------------------------
	
	/**
	 * 
	 * <pre>
	 * @method notFound
	 * @statuscode 404 [Not Found]
	 * @contents 서버는 요청받은 리소스를 찾을 수 없습니다. 
	 * 브라우저에서는 알려지지 않은 URL을 의미합니다. 
	 * 이것은 API에서 종점은 적절하지만 리소스 자체는 존재하지 않음을 의미할 수도 있습니다. 
	 * 서버들은 인증받지 않은 클라이언트로부터 리소스를 숨기기 위하여 
	 * 이 응답을 403 대신에 전송할 수도 있습니다. 
	 * 이 응답 코드는 웹에서 반복적으로 발생하기 때문에 가장 유명할지도 모릅니다.
	 * </pre>
	 * 
	 * @param msg
	 * @return
	 * @author James Choi
	 */
	public Response notFound(String msg) {
		return Response.status(Response.Status.NOT_FOUND)
				   .type(MediaType.APPLICATION_JSON)
				   .entity(new MessageResponse(msg))
				   .build();
	}
	
	/**
	 * 
	 * <pre>
	 * @method notFound
	 * @statuscode 404 [Not Found]
	 * @contents 서버는 요청받은 리소스를 찾을 수 없습니다. 
	 * 브라우저에서는 알려지지 않은 URL을 의미합니다. 
	 * 이것은 API에서 종점은 적절하지만 리소스 자체는 존재하지 않음을 의미할 수도 있습니다. 
	 * 서버들은 인증받지 않은 클라이언트로부터 리소스를 숨기기 위하여 
	 * 이 응답을 403 대신에 전송할 수도 있습니다. 
	 * 이 응답 코드는 웹에서 반복적으로 발생하기 때문에 가장 유명할지도 모릅니다.
	 * </pre>
	 * 
	 * @param response
	 * @return
	 * @author James Choi
	 */
	public Response notFound(JerseyResourceResponse response) {
		return Response.status(Response.Status.NOT_FOUND)
				   .type(MediaType.APPLICATION_JSON)
				   .entity(response)
				   .build();
	}
	
	// ---------------------------------------------------------------------------------------
	
	/**
	 * 
	 * <pre>
	 * @method conflict
	 * @statuscode 409 [Conflict]
	 * @contents 이 응답은 요청이 현재 서버의 상태와 충돌될 때 보냅니다.
	 * </pre>
	 * 
	 * @param msg
	 * @return
	 * @author James Choi
	 */
	public Response conflict(String msg) {
		return Response.status(Response.Status.CONFLICT)
				   	   .type(MediaType.APPLICATION_JSON)
				   	   .entity(new MessageResponse(msg))
				   	   .build();
	}
	
	/**
	 * 
	 * <pre>
	 * @method conflict
	 * @statuscode 409 [Conflict]
	 * @contents 이 응답은 요청이 현재 서버의 상태와 충돌될 때 보냅니다.
	 * </pre>
	 * 
	 * @param response
	 * @return
	 * @author James Choi
	 */
	public Response conflict(JerseyResourceResponse response) {
		return Response.status(Response.Status.CONFLICT)
					   .type(MediaType.APPLICATION_JSON)
					   .entity(response)
					   .build();
	}
	
	
}
