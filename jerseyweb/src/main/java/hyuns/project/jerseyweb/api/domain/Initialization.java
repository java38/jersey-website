package hyuns.project.jerseyweb.api.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "JERSEY000")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Initialization {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SEQ", nullable = true) 
	private long seq;

	@CreationTimestamp
	@Column(name = "RGDT")
	private LocalDateTime regDate;
	
	@Column(name = "SS", nullable = false)
	private String ss;
	
	// Constructor
	public Initialization(String ss) 
	{
		this.ss = ss;
	}
}
