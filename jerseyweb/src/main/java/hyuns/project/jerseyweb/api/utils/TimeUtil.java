package hyuns.project.jerseyweb.api.utils;

import java.time.Duration;
import java.time.Instant;

public class TimeUtil {
	
//	private static final Logger log = LogManager.getLogger(TimeUtil.class);
	private Instant start;
	
	public TimeUtil() {
		this.start = Instant.now();
//		log.debug("start time: " + this.start);
	}
	
	public long elapsed() {
		return Duration.between(start, Instant.now()).toMillis();
	}
	
	
	
	
}
