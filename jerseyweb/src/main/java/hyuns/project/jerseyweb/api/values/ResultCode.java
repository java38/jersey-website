package hyuns.project.jerseyweb.api.values;

import lombok.Getter;

/**
 * 
 * <pre> 
 * hyuns.project.jerseyweb.api.values
 * ApplicationResultCode.java
 * </pre>
 *
 * @author James Choi
 * @date Mar 1, 2020
 * @since 1.0
 *
 */
@Getter
public enum ResultCode {
	
	Test("Test", "test"),
	
	/**
	 * @type Common
	 * @contents Server Error
	 * @author James Choi
	 */
	Common0000("Common0000", "server error"),
	
	/**
	 * @type Common
	 * @contents Invalid Param
	 * @author James Choi
	 */
	Common0001("Common0001", "invalid param")
	;
	
	
	private String code;
	private String msg;
	
	/**
	 * 
	 * @param code
	 * @param msg
	 * @author James Choi
	 */
	private ResultCode(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}
}
