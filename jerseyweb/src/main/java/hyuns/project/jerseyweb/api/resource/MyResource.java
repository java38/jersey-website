package hyuns.project.jerseyweb.api.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import hyuns.project.jerseyweb.api.JerseyResource;
import hyuns.project.jerseyweb.api.response.MessageResponse;
import hyuns.project.jerseyweb.api.utils.TimeUtil;
import hyuns.project.jerseyweb.api.values.ResultCode;

/**
 * Root resource (exposed at "testresource" path)
 */
@Path("testresource")
public class MyResource extends JerseyResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getIt() {
    	TimeUtil time = new TimeUtil();
		
		try {
			System.out.println("test");
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			System.out.println(time.elapsed() + "ms");
		}
    	
        return defaultSuccess(new MessageResponse(ResultCode.Test.getMsg()));
    }
}
