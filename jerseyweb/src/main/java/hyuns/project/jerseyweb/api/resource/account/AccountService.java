package hyuns.project.jerseyweb.api.resource.account;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import hyuns.project.jerseyweb.api.JerseyResource;
import hyuns.project.jerseyweb.api.values.ResultCode;

/**
 * 
 * <pre> 
 * hyuns.project.jerseyweb.api.resource.account
 * AccountService.java
 * </pre>
 *
 * @since 1.0
 * @date Mar 1, 2020
 * @author James Choi
 */
public abstract class AccountService extends JerseyResource {

	private static final Logger log = LogManager.getLogger(AccountService.class);
	
	/**
	 * 
	 * <pre>
	 * @constructor AccountService (Default)
	 * </pre>
	 *
	 * @author James Choi
	 */
	public AccountService() {
	}
	
	
	public Response signUp(@Context HttpServletRequest request,
						   AccountSignUpParam param) 
	{
		final Locale locale = request.getLocale();
		log.info("[Language] ==>" + locale.getLanguage());
		log.info("[Country] ==>" + locale.getCountry());
		
		if ( !param.check() )
			return badRequest(new AccountSignUpResponse(locale, ResultCode.Common0001));
		
		
		
		return null;
	}
	
}
