package hyuns.project.jerseyweb.api.resource.account;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonProperty;

import hyuns.project.jerseyweb.api.resource.param.ServiceParam;

/**
 * 
 * <pre> 
 * hyuns.project.jerseyweb.api.resource.account
 * AccountSignUpParam.java
 * </pre>
 *
 * @since 1.0
 * @date Mar 1, 2020
 * @author James Choi
 */
public class AccountSignUpParam implements ServiceParam {

	private static final Logger log = LogManager.getLogger(AccountSignUpParam.class);
	
	@JsonProperty
	private String id;
	
	@JsonProperty
	private String passwordHashBase64;
	
	@Override
	public boolean check() {
		log.info(this.toString());
		
		if (StringUtils.isEmpty(id) || StringUtils.isEmpty(passwordHashBase64) )
			return false;
		
		return true;
	}
	
}
