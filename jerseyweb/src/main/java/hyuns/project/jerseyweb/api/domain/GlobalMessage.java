package hyuns.project.jerseyweb.api.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Table(name = "JERSEY001")
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED) 
@Getter
public class GlobalMessage {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SEQ", nullable = false)
	private long seq;
	
	@CreationTimestamp
	@Column(name = "RGDT") 
	private LocalDateTime regDate;
	
	@Column(name = "CODE", nullable = false)
	@NonNull private String code;
	
	@Column(name = "EN", nullable = false)
	@NonNull private String en;
	
	@Column(name = "KO", nullable = false)
	@NonNull private String ko;
}
