package hyuns.project.jerseyweb.api.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;

import hyuns.project.jerseyweb.api.JerseyResource;

/**
 * 
 * <pre> 
 * hyuns.project.jerseyweb.api.resource
 * PageResource.java
 * </pre>
 *
 * @since 1.0
 * @date Mar 1, 2020
 * @author James Choi
 */
@Path("page")
public class PageResource extends JerseyResource {

	/**
	 * 
	 * <pre>
	 * @method signupPage
	 * </pre>
	 * 
	 * @return Viewable object(sign up)
	 * @author James Choi
	 */
	@Path("signup")
	@GET
	public Response signupPage() {
		return defaultSuccess(new Viewable("/signup.jsp"));
		
	}
	
	/**
	 * 
	 * <pre>
	 * @method mainPage
	 * </pre>
	 * 
	 * @return Viewable object(main)
	 * @author James Choi
	 */
	@Path("main")
	@GET
	public Response mainPage() {
		return defaultSuccess(new Viewable("/main.jsp"));
	}
}
