package hyuns.project.jerseyweb;

import java.security.Security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;

import hyuns.project.jerseyweb.init.JerseyApplicationResultCode;
import hyuns.project.jerseyweb.init.Jpa;

public class JerseyApplicationEventListener implements ApplicationEventListener {

	private static final Logger log = LogManager.getLogger(JerseyApplicationEventListener.class);
	
	@Override
	public void onEvent(ApplicationEvent event) {
		switch (event.getType()) {
		case INITIALIZATION_START:
			log.info("[Event] - INITIALIZATION_START");
			break;
		
		case INITIALIZATION_APP_FINISHED:
			log.info("[Event] - INITIALIZATION_APP_FINISHED");
			break;
		
		case INITIALIZATION_FINISHED:
			log.info("[Event] - INITIALIZATION_FINISHED");
			this.doWhenInitializationFinished();
			break;
			
		case DESTROY_FINISHED:
			log.info("[Event] - DESTROY_FINISHED");
			this.doWhenDestroyFinished();
			break;

		case RELOAD_FINISHED:
			log.info("[Event] - RELOAD_FINISHED");
			break;	
			
		default:
			log.info("[Event] - ?");
			break;
		}
	}

	@Override
	public RequestEventListener onRequest(RequestEvent requestEvent) {
		return null;
	}

	private void doWhenInitializationFinished() {
		try {
			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
			log.info("[Bouncy Castle] Add Provider - ok");
			
			Jpa.Initialize("jerseywebproject");
			log.info("[JPA] Initialize - ok");
			
			JerseyApplicationResultCode.init();
			log.info("[JerseyApplicationResultCode] Initialize - ok");
			
			
		} catch (Exception e) {
			doWhenDestroyFinished();
			log.error("doWhenInitializationFinished()", e);
		}
	}
	
	
	private void doWhenDestroyFinished() {
		try {
			
		} catch (Exception e) {
			log.error("doWhenDestroyFinished()", e);
		}
	}
}
