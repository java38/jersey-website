package hyuns.project.jerseyweb;

import javax.inject.Singleton;
import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;
import org.glassfish.jersey.servlet.ServletProperties;

@ApplicationPath("/")
@Singleton
public class AppConfig extends ResourceConfig {
	
	public AppConfig() {
		// WebPage Static Asset
		register(ServletProperties.class);		
		property(ServletProperties.FILTER_STATIC_CONTENT_REGEX, "/assets/*/.js");
		property(ServletProperties.FILTER_STATIC_CONTENT_REGEX, "/assets/*/.css");
		
		
		// Project Packages
		packages("hyuns.project.jerseyweb.api");
		
		// Jersey Framework Event
		register(JerseyApplicationEventListener.class);
		
		// Jersey MultiPart
		register(MultiPartFeature.class);
		
		// WebPage JSP MVC
		register(JspMvcFeature.class);
		property(JspMvcFeature.TEMPLATE_BASE_PATH, "/WEB-INF/views");
	}
	
}
