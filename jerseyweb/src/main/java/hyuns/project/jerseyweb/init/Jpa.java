package hyuns.project.jerseyweb.init;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * <pre> 
 * hyuns.project.jerseyweb.init
 * Jpa.java
 * </pre>
 *
 * @since 1.0
 * @date Mar 1, 2020
 * @author James Choi
 */
public class Jpa {

	private static final Logger log = LogManager.getLogger(Jpa.class);
	private static EntityManagerFactory entityManagerFactory;
	
	/**
	 * 
	 * <pre>
	 * @method Initialize
	 * @contents Java Persistence API
	 * </pre>
	 * 
	 * @param persistenceUnitName
	 * @author James Choi
	 */
	public final static void Initialize(String persistenceUnitName) {
    	
    	if (entityManagerFactory == null) {
    		log.info("entity manager factory is null !!!");
    		log.info("persistenceUnitName: " + persistenceUnitName);
    		entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnitName);
    	}
    	else
    		log.info("entity manager factory is not null.");
    }
	
	/**
	 * 
	 * <pre>
	 * @method getEntityManagerFactory
	 * </pre>
	 * 
	 * @return
	 * @author James Choi
	 */
	public final static EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}
}
