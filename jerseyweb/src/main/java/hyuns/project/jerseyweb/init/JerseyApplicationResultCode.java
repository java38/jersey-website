package hyuns.project.jerseyweb.init;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import hyuns.project.jerseyweb.api.domain.GlobalMessage;
import hyuns.project.jerseyweb.api.utils.TimeUtil;

public class JerseyApplicationResultCode {

	private static final Logger log = LogManager.getLogger(JerseyApplicationResultCode.class);
	
	private static final String LANG_KO = "ko";
	
	private static HashMap<String, String> enSeqMsgMap = new HashMap<String, String>();
	private static HashMap<String, String> koSeqMsgMap = new HashMap<String, String>();
	
	private static HashMap<String, String> enCodeMsgMap = new HashMap<String, String>();
	private static HashMap<String, String> koCodeMsgMap = new HashMap<String, String>();
		
	/**
	 * 
	 * <pre>
	 * @method init
	 * @contents Global Message Data Initialize
	 * </pre>
	 * 
	 * @author James Choi
	 */
	public static void init() {
		TimeUtil timeCheck = new TimeUtil();
		
		EntityManagerFactory emf = Jpa.getEntityManagerFactory();
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		
		try {
			List<GlobalMessage> globalMessages = em.createQuery(
					"select g from GlobalMessage as g", 
					GlobalMessage.class).getResultList();
			for (GlobalMessage globalMessage : globalMessages) {
				log.debug("code:" + globalMessage.getCode());
				log.debug("ko:" + globalMessage.getKo());
				log.debug("en:" + globalMessage.getEn());
				
				final long seq = globalMessage.getSeq();
				final String code = globalMessage.getCode();
				final String en = globalMessage.getEn();
				final String ko = globalMessage.getKo();
				
				enSeqMsgMap.put(String.valueOf(seq), en);
				koSeqMsgMap.put(String.valueOf(seq), ko);
				
				enCodeMsgMap.put(code, en);
				koCodeMsgMap.put(code, ko);
				
				log.debug("seq: " + seq + " / code: " + code + " / ko: " + ko + " / en: " + en);
			}

		} catch (Exception e) {
			e.printStackTrace();
			
			// Rollback;
			tx.rollback();
		} finally {
			// EntityManager Close;
			em.close();
			log.info(timeCheck.elapsed() + "ms");
		}
	}
	
	/**
	 * 
	 * <pre>
	 * @method getMessage
	 * </pre>
	 * 
	 * @param locale
	 * @param msgCode
	 * @return
	 * @author James Choi
	 */
	public static String getMessage(Locale locale, String messageCode) {
		final String lang = locale.getLanguage();
		return getData(lang, messageCode);
	}
	
	/**
	 * 
	 * <pre>
	 * @method getData
	 * 
	 * </pre>
	 * 
	 * @param lang
	 * @param msgCode
	 * @return
	 * @author James Choi
	 */
	public static String getData(String lang, String messageCode) {
		String msg = null;
		
		if ( LANG_KO.equals(lang) )
			msg = koCodeMsgMap.get(messageCode);
		else
			msg = enCodeMsgMap.get(messageCode);
		
		return null == msg ? "?" : msg;
	}
}
