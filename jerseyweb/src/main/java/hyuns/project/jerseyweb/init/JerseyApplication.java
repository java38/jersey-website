package hyuns.project.jerseyweb.init;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import hyuns.project.jerseyweb.api.domain.Initialization;
import hyuns.project.jerseyweb.api.utils.TimeUtil;

public class JerseyApplication {

	private static final Logger log = LogManager.getLogger(JerseyApplication.class);
	
	public static final String characterEncoding  = "UTF-8";
	public String ss;
	
	public JerseyApplication() {
		TimeUtil time = new TimeUtil();
		
		EntityManagerFactory emf = Jpa.getEntityManagerFactory();
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		
		try {
			Initialization init = em.find(Initialization.class, 1L);
			
			if (init.getSeq() != 1L)
				log.error("init - Fali!");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// EntityManager Close;
			em.close();
		}
		
		time.elapsed();
	}
	
	private static class LazyHolder {
		public static final JerseyApplication singleton = new JerseyApplication();
	}
	
	public static JerseyApplication getInstance() {
		return LazyHolder.singleton;
	}
	
}
